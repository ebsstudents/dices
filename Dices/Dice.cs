﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dices
{
    class Dice
    {

       

     #region fields
        int value;

        public int Value
        {
            get { return this.value; }
            set { this.value = value; }
        }

        int x;

        public int X
        {
            get { return x; }
            set { x = value; }
        }
        int y;

        public int Y
        {
            get { return y; }
            set { y = value; }
        }

    #endregion

        public void Roll() 
        {
            value = RandomNumbers.Dice();
        }
    }
}
