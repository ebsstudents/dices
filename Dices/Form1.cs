﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dices
{
    public partial class Form1 : Form
    {

        List<Dice> dices = new List<Dice>();

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            dices = GenerateMockDices(64);
            ShowDices(dices);
        }

        private void ShowDices(List<Dice> dices)
        {
            foreach (var dice in dices)
            {
                ShowDice(dice);
            }
        }
 
        private void ShowDice(Dice dice)
        {
            Label diceLabel = new Label();

            diceLabel.Left = dice.Y;
            diceLabel.Top = dice.X;
            diceLabel.Text = dice.Value.ToString();

            diceLabel.BackColor = Color.White;
            diceLabel.BorderStyle = BorderStyle.FixedSingle;
            diceLabel.Font = new Font("Microsoft Sans Serif", 30F, FontStyle.Regular);
            diceLabel.Size = new Size(44, 48);
            diceLabel.Tag = dice;

            diceLabel.Click += ReRollDice;
            diceLabel.MouseMove += ReRollDice;


            this.Controls.Add(diceLabel);
        }


        private void ReRollDice(object sender, EventArgs e)
        {
            var dice = ((Dice)(((Label)sender).Tag));
            dice.Roll();
            ((Label)sender).Text = dice.Value.ToString();
        }

        private static List<Dice> GenerateMockDices(int numberOfDices)
        {
            List<Dice> dices = new List<Dice>();
            for (int i = 0; i < numberOfDices; i++)
            {
                dices.Add(new Dice {    Value = RandomNumbers.Dice(), 
                                        X = 50 + 50*(i/8), 
                                        Y = 20 + 50*(i%8)});
            }
            return dices;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            foreach (var dice in dices)
            {
                dice.Roll();
            }
            HideDices();
            ShowDices(dices);
        }

        private void HideDices()
        {
            foreach (var control in Controls)
            {
                if (control is Label)
                {
                    ((Label)control).Visible = false;
                }
            }
        }



        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
